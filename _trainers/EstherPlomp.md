---
title: Esther Plomp
excerpt: Trainer in Training
header:
   teaser: ../assets/images/trainer_images/EstherPlomp.jpg
sidebar:
   - title: ''
     image: ../assets/images/trainer_images/EstherPlomp.jpg
     image_alt: Esther
---

### Topics:

Open Science, Data Literacy

### Background:

Esther has been the Data Steward of the Faculty of Applied Sciences
since 2018. She has provided trainings for research groups, PhD
candidates, and is a certified Carpentries trainer.

### Training style:

Workshops, seminars, flipped classroom

City: The Hague <br/> Country: the Netherlands <br/> ☑ Happy to offer
services online <br/> ➢ I am happy to travel
