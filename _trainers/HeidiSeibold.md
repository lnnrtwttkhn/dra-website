---
title: "Heidi Seibold"
excerpt: "Open and Reproducible Data Science"
header:
  teaser: "/assets/images/trainer_images/HeidiSeibold.jpg"
sidebar:
  - title: "Homepage"
    image: "/assets/images/trainer_images/HeidiSeibold.jpg"
    image_alt: "Heidi"
    text: "[heidiseibold.com](https://heidiseibold.com)"
  - title: "Contact"
    text: "[heidi@heidiseibold.de](mailto:heidi@heidiseibold.de)"
  - title: "Languages"                       
    text: "English, German"
---
### Topics:
Reproducibility, Open Science, Research Culture, Research Software Engineering 

### Research background: 
Machine Learning, Statistics, Health Research

### Training style: 
All trainings are interactive and no workshop is exactly like the other,
because I adapt it to you and your needs. 

### Additional info: 
I write a [newsletter about Open and Reproducible Data Science](https://heidiseibold.ck.page/) and am happiest when on a bicycle.


City: Munich <br/> 
Country: Germany <br/>
&#x2611; Happy to offer services virtually <br/>
&#x2611; Happy to travel to places I can reach by train <br/>
{: .notice}
