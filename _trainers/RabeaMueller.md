---
title: Rabea Müller
excerpt: Trainer in Training
header:
   teaser: ../assets/images/trainer_images/RabeaMueller.jpg
sidebar:
   - title: ''
     image: ../assets/images/trainer_images/RabeaMueller.jpg
     image_alt: Rabea
---

### Topics:

Open Science, Data Literacy, Data Science

### Background:

I completed my Instructor certification from The Carpentries and learned
about evidence-based teaching methods and have taught over 50 workshops
since then. I also received the trainer certificate from the Carpentries
to teach core concepts in educational psychology and instructional
design and their practical application. Besides teaching, I organize our
workshops, develop new teaching content and coordinate our instructors
at ZB MED. The Carpentries and ZB MED have always supported me in my
desires, so I can do exactly what I love to do. Giving people new
concepts that are in the spirit of Open Science with open tools and Open
Educational Resources.

### Training style:

Live Coding, Hands-on-Workshops, Video Introductions

### Additional info:

In keeping with my profession today, I have always been a “PC kid” and
loved playing games on the PC as a child and still love to gamble today,
althouh I switched to console. To balance it out, I also like to be out
in nature without a plan, preferably with my e-mountain bike. And when
I’m not doing any of that, I lie on the couch with my cat and learn from
her that laziness is a virtue, not only in programming but also in real
life.

City: Cologne <br/> Country: Germany <br/> ☑ Happy to offer services
online <br/> ➢ Travel restrictions: Would much prefer anything online.
Exception are courses in Cologne.
