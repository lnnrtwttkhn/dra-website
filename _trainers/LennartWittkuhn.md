---
title: Lennart Wittkuhn
excerpt: Trainer in Training
header:
   teaser: ../assets/images/trainer_images/LennartWittkuhn.jpg
sidebar:
   - title: ''
     image: ../assets/images/trainer_images/LennartWittkuhn.jpg
     image_alt: Lennart
---

### Topics:

Open Science, Data Literacy, Research Software Engineering,
Computational Reproducibility

### Background:

Cognitive Neuroscientist & Psychologist with 5+ years experience in
experimental neuroscience, large-scale data analysis, computation,
machine learning, research software engineering, lab management,
computational reproducibility and open science

### Training style:

Interactive workshops, seminars, lectures, demonstrations

City: Hamburg <br/> Country: Germany <br/> ☑ Happy to offer services
online <br/> ➢ I am happy to travel (No flying, Europe only)
