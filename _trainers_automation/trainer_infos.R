## ------ Trainer socials
library("googlesheets4")

## Read in data
trns <- read_sheet("https://docs.google.com/spreadsheets/d/1bQ9q1uRT8YY3nbkBx3q5DCG8kPC6eXhih6X3Xa8h54o/edit?resourcekey#gid=223589169")

trns$Mastodon
trns$LinkedIn

## ------ Trainer photos as collage 
library("magick")

## Read images
folder_imgs <- "../assets/images/trainer_images/"
files_trn_img <- list.files(folder_imgs)
files_trn_img <- files_trn_img[files_trn_img != "trainer_collage.jpg"]
trn_imgs <- image_read(paste0(folder_imgs, files_trn_img))

## Resize images
trn_imgs <- image_scale(trn_imgs, "x400")
trn_info <- image_info(trn_imgs)

wide_imgs <- trn_imgs[which(trn_info$width > 500)]
cropped_imgs <- image_crop(wide_imgs, "400x400+70+30")
trn_imgs[which(trn_info$width > 500)] <- cropped_imgs

## Create collage
trn_mntg <- image_montage(trn_imgs, "400x400+0+0", tile = "4x3", bg = "#e7f1e7")
trn_mntg

## Save
image_write(trn_mntg, path = paste0(folder_imgs, "trainer_collage.jpg"), format = "jpeg")
