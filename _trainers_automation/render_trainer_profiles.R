library("googlesheets4")
library("googledrive")
library("quarto")

## Read in data
trns <- read_sheet("https://docs.google.com/spreadsheets/d/1bQ9q1uRT8YY3nbkBx3q5DCG8kPC6eXhih6X3Xa8h54o/edit?resourcekey#gid=223589169")
accepted_trainers <- c("JoyceKao")


## Preprocessing 
### Column names should not have special characters, replace space with _
names(trns) <- gsub(pattern = "[^a-zA-Z0-9 ]", replacement = "", names(trns))
names(trns) <- gsub(pattern = " ", replacement = "_", names(trns))

### Remove special characters and unreasonable first names ;)
trns$First_Name <- gsub(pattern = " |[^a-zA-Z0-9 ]", "", trns$First_Name)
trns$First_Name <- gsub(pattern = "DanielofficialDannypreferred", "Danny", trns$First_Name)

### Create a combined first and last name for file names (image + profile md)
name <- paste0(trns$First_Name, trns$Last_Name)
name <- gsub(pattern = "ü", "ue", name)
name <- gsub(pattern = " |[^a-zA-Z0-9 ]", "", name)
trns$name <- name

## preprocess content to make it easier to run the quarto file
trns$additional <- !is.na(trns$Additional_remarks_about_yourself_eg_fun_fact_hobbies_other_activities)
trns$travel_restrictions <- !is.na(trns$Are_there_any_restrictions_on_travel_ie_distance_mode_of_travel_etc)


#' Create profile md and add image to the repo
#'
#' @param trn line in the trainers data set that was generated via a Google form.
create_profile <- function(trn) {
  
  
  ## create draft of md-file
  out <- paste0(trn$name, ".md")
  trn_params <- as.list(trn)
  
  quarto_render("create_trainer_profile.qmd", output_file = out, output_format = "commonmark",
                execute_params = list(trn = trn))
  
  ## get profile image meta data
  image_link <- trn$Please_upload_a_profile_picture_of_yourself_for_your_trainer_profile_on_the_DRA_website
  if(!is.na(image_link)) {
    image_metadata <- drive_get(image_link)
    image_type <- gsub(".*\\.", "", image_metadata$name)
    
    ## download profile image and save
    image_path <- paste0("../assets/images/trainer_images/", trn$name, ".", image_type)
    try(drive_download(image_link, path = image_path, overwrite = TRUE))
  } else {
    image_path <- "../assets/images/DRA_Logo/DRA_LogoColourPos.jpg"
  }
  
  
  ## add header needed for Jekyll page
  existing_content <- readLines(out)
  excerpt <- ifelse(trn$name %in% accepted_trainers,
                    trn$What_broad_areas_would_your_training_be_under_Check_all_that_apply,
                    "Trainer in Training")
  new_lines <- c("---",
                 paste("title:", trn$First_Name, trn$Last_Name), 
                 paste("excerpt:", excerpt),
                 "header:",
                 paste("   teaser:", image_path),
                 "sidebar:",
                 "   - title: ''",
                 paste("     image:", image_path), 
                 paste("     image_alt:", trn$First_Name),
                 "---"
  )
  updated_content <- c(new_lines, existing_content)
  
  ## Save to _trainers folder
  writeLines(updated_content, paste0("../_trainers/", out))
  
  ## Remove first draft
  file.remove(out)
}

trn <- trns[11, ]
create_profile(trn)

## Create profile for all who filled in the form
apply(trns, 1, create_profile)
