---
permalink: /contact/
layout: splash
header:
  overlay_color: "#7ca681"
author_profile: false
title: "Contact"
---

The Digital Research Academy is currently being built with a small team.

To get in touch, send a message to [Heidi Seibold](mailto:dra@heidiseibold.de).

Interested in updates from the Digital Research Academy? Sign up for our mailing list!
[Subscribe to mailing list](https://www.listserv.dfn.de/sympa/subscribe/dra){: .btn .btn--primary}

<img src="/assets/images/DRA_Logo/DRA_LogoColourPos.png" alt="Digital Research Academy Logo." width="300" />
