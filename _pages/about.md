---
permalink: /about/
author_profile: true
title: "About"
---


<img src="/assets/images/DRA_Logo/DRA_LogoColourPos.png" alt="Digital Research Academy Logo." width="150" style="float:right"/>

The Digital Research Academy is a grassroots trainer network.

We offer training with the goal of **improving the quality of research**.


We are organised like a fruit:

- Core: core team
- Pulp: trainers

<img src="/assets/images/icon-fruit.png" alt="A peach, cut open with a core." width="200"/>

# Core team

- Heidi Seibold
- Joyce Kao
- Melanie Imming

# Trainers

We are currently building up the trainer community.

[Join us!](/trainers-join){: .btn .btn--primary}

